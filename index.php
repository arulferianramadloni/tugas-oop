<?php
    require_once('animal.php');
    require_once('ape.php');
    require_once('frog.php');

    $sheep = new Animal("shaun");
    echo "<br> Name : ". $sheep->name . "<br>"; // "shaun"
    echo "legs : ". $sheep->legs. "<br>"; // 4
    echo "cold blooded : ". $sheep->cold_blooded . "<br>"; // "no"

    $kodok = new Frog("buduk");
    echo "<br> Name : ". $kodok->name . "<br>";
    echo "legs : ". $kodok->legs. "<br>";
    echo "cold blooded : ". $kodok->cold_blooded . "<br>";
    $kodok->jump() ;


    $sungokong = new Ape("kera sakti");
    echo "<br> <br> Name : ". $sungokong->name . "<br>";
    echo "legs : ". $sungokong->legs. "<br>";
    echo "cold blooded : ". $sungokong->cold_blooded . "<br>";
    $sungokong->yell();

?>